package main

import (
	"fmt"
	"log"
	"os"
	"reflect"

	"gopkg.in/yaml.v3"
)

func init() {
	RegisterRunnerHandler("GoRunner", NewGoRunner)
	RegisterRunnerHandler("RubyRunner", NewRubyRunner)
}

func main() {
	yaml_content := `
runners:
- runnerType: GoRunner
  name: runner0
  go_specific: value0
- runnerType: RubyRunner
  name: runner1
  ruby_specific: value1
`

	var config Cfg
	err := yaml.Unmarshal([]byte(yaml_content), &config)
	if err != nil {
		log.Printf(`Failed to decode config yml, err = %v`, err)
		os.Exit(1)
	}

	runners, err := collectRunnersFromYamlConfig(config)
	if err != nil {
		log.Printf("error: %v\n", err)
		os.Exit(1)
	}

	for _, runner := range runners {
		runner.Run()
	}

	log.Println(`OK`)
}

type Runner interface {
	Run() error
}

type UnmarshalCfg interface {
	UnmarshalCfg(n yaml.Node) error
}

type Cfg struct {
	Runners []yaml.Node `yaml:"runners"`
}

type BaseRunnerCfg struct {
	Type string `yaml:"runnerType"`
	Name string `yaml:"name"`
}

func NewGoRunner(name string) Runner {
	return &GoRunner{Name: name}
}

type GoRunner struct {
	Name string // TODO: nested structs
	Cfg  GoRunnerCfg
}

type GoRunnerCfg struct {
	SpecificGo string `yaml:"go_specific"`
}

func (r *GoRunner) Run() error {
	log.Printf("%#v", r)

	return nil
}

func (r *GoRunner) UnmarshalCfg(n yaml.Node) error {
	var cfg GoRunnerCfg
	if err := n.Decode(&cfg); err != nil {
		return err
	}

	r.Cfg = cfg
	return nil
}

func NewRubyRunner(name string) Runner {
	return &RubyRunner{Name: name}
}

type RubyRunner struct {
	Name string // TODO: nested structs
	Cfg  RubyRunnerCfg
}

type RubyRunnerCfg struct {
	SpecificRuby string `yaml:"ruby_specific"`
}

func (r *RubyRunner) Run() error {
	log.Printf("%#v", r)

	return nil
}

func (r *RubyRunner) UnmarshalCfg(n yaml.Node) error {
	var cfg RubyRunnerCfg
	if err := n.Decode(&cfg); err != nil {
		return err
	}

	r.Cfg = cfg
	return nil
}

func collectRunnersFromYamlConfig(config Cfg) ([]Runner, error) {
	var runners []Runner

	for _, runnerNode := range config.Runners {
		var baseRunnerCfg BaseRunnerCfg
		err := runnerNode.Decode(&baseRunnerCfg)
		if err != nil {
			return runners, fmt.Errorf(`Failed to decode runner's basic info, err = %v`, err)
		}
		log.Printf("%#v\n", baseRunnerCfg)

		runner, err := createRunner(baseRunnerCfg.Type, baseRunnerCfg.Name, runnerNode)
		if err != nil {
			return runners, fmt.Errorf(`Failed to create Runner %v: %v`, baseRunnerCfg.Name, err)
		}

		u, ok := runner.(UnmarshalCfg)
		if !ok {
			return runners, fmt.Errorf(`Runner %v must implement interface UnmarshalCfg`, baseRunnerCfg.Name)
		}

		err = u.UnmarshalCfg(runnerNode)
		if err != nil {
			return runners, fmt.Errorf(`Failed to configure Runner %v: %v`, baseRunnerCfg.Name, err)
		}

		runners = append(runners, runner)
	}

	return runners, nil
}

func NewRunnerHandlersRegistry() map[string]func(string) Runner {
	return make(map[string]func(string) Runner)
}

func createRunner(typeStr, name string, yamlCfg yaml.Node) (Runner, error) {
	var runner Runner

	h := GetRunnerHandler(typeStr)
	if h == nil {
		return runner, fmt.Errorf(`No handler registered for %v`, typeStr)
	}

	f := reflect.ValueOf(h)

	in := make([]reflect.Value, 1)
	in[0] = reflect.ValueOf(name)

	res := f.Call(in)

	runner = res[0].Interface().(Runner)
	return runner, nil
}

var defaultRunnerHandlersRegistry = NewRunnerHandlersRegistry()

func RegisterRunnerHandler(name string, f func(string) Runner) {
	defaultRunnerHandlersRegistry[name] = f
}

func GetRunnerHandler(name string) func(string) Runner {
	return defaultRunnerHandlersRegistry[name]
}
